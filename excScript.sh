# excScript
# replace Name with your name 
# the symbol # is used to add a comment in the shell
n=1
for N in 0.1 1 5
do
	echo "Creating Name$n folder with N=$N" 
	#printing a message descibing what is done in each iteration
	mkdir Name$n
	#creating a new folder Name by appending the number n to the name
        #the curly br. must be added if the variable name is more than one character	
	#cd Name${n}
        # changing the directory to the new folder	
	#cp ../inputOrig.txt ./
	# copying the files inputOrig.txt input.txt from the initial folder (scriptmkdir) into the new sub folder Name$n
	#cp ../input.txt ./
	sed -e "s/xxxx/${N}/" inputOrig.txt > input.txt
	cp input.txt Name$n/
	#here you can call an executable that will run your simulation using the input parameter
	#cd ../
	n=$(( $n + 1 ))
done
