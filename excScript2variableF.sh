# excScript
# replace Name with your name 
# the symbol # is used to add a comment in the shell
n=1
for N in 0.1 1 5
do
	m=1
	for M in 2 3
	do
		echo "Creating Name_${n}_${m} folder with N=$N and M=$M"
	       	#printing a message descibing what is done in each iteration
		mkdir Name_${n}_${m}
		#creating a new folder Name by appending the number n to the name
		#the curly br. must be added if the variable name is more than one character
	        sed -e "s/xxxx/${N}/" -e "s/yyyy/${M}/" input2vOrig.txt > input.txt
		cp input.txt Name_${n}_${m}/
		#here you can call an executable that will run your simulation using the input paramete
		#cd ../
		m=$(( $m + 1 ))
	done
	n=$(($n+1))
done
